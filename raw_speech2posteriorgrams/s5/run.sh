#!/bin/bash

#
# Copyright 2013 Bagher BabaAli,
#           2014 Brno University of Technology (Author: Karel Vesely)
#
# TIMIT, description of the database:
# http://perso.limsi.fr/lamel/TIMIT_NISTIR4930.pdf
#
# Hon and Lee paper on TIMIT, 1988, introduces mapping to 48 training phonemes, 
# then re-mapping to 39 phonemes for scoring:
# http://repository.cmu.edu/cgi/viewcontent.cgi?article=2768&context=compsci
#

. ./cmd.sh 
. ./path.sh

feats_nj=10
train_nj=20
decode_nj=3
stage=0

timit=/home/leo/timit/timit  # @LEO
cu_chloe=/home/leo/CU-CHLOE  # @LEO

if [ $stage -le 0 ]; then
  echo ============================================================================
  echo "                            Data Preparation                              "
  echo ============================================================================

  local/data_prep.sh $timit $cu_chloe || exit 1
  echo "Data Preparation Succeed!"

fi

if [ $stage -le 1 ]; then
  echo ============================================================================
  echo "                     MFCC Feature Extration & CMVN                        "
  echo ============================================================================

  # # Now make MFCC features.
  mfccdir=mfcc

  for x in train dev test; do 
    steps/make_mfcc.sh --cmd "$train_cmd" --nj $feats_nj data/$x exp/make_mfcc/$x $mfccdir
    steps/compute_cmvn_stats.sh data/$x exp/make_mfcc/$x $mfccdir
  done
  echo "MFCC Feature Extration & CMVN Succeed!"
fi

if [ $stage -le 2 ]; then
  echo ============================================================================
  echo "                 Make Canonical Transcriptions                         "
  echo ============================================================================
# make canonical transcriptions by forced alignment
  for x in train dev; do
    local/align_si.sh --boost-silence 1.25 --nj "$train_nj" --cmd "$train_cmd" \
    data/${x} data/lang exp/mono exp/mono_ali_${x} || exit 1
  done
  exit
fi

if [ $stage -le 3 ]; then
  echo ============================================================================
  echo "                            APM Training                                  "
  echo ============================================================================

  local/nnet/run_dnn.sh || exit 1
  echo "APM Training finished!"
  exit
fi

if [ $stage -le 4 ]; then
  echo ============================================================================
  echo "                      Benchmark APM by decoding on dev set                "
  echo ============================================================================
  dataset=data/test
  # rm -rf $dataset/split*

  # generate duration file
  wav-to-duration scp:$dataset/wav.scp ark,t:$dataset/dur.ark || exit 1
  echo "duration file complete!"

  python local/phn_sequence_text.py $dataset || exit 1
  # generate stm file
  awk -v dur=$dataset/dur.ark \
  'BEGIN{ 
   while(getline < dur) { durH[$1]=$2; } 
   print ";; LABEL \"O\" \"Overall\" \"Overall\"";
   print ";; LABEL \"F\" \"Female\" \"Female speakers\"";
   print ";; LABEL \"M\" \"Male\" \"Male speakers\""; 
  } 
  { wav=$1; spk=gensub(/_.*/,"",1,wav); $1=""; ref=$0;
   gender=(substr(spk,0,1) == "f" ? "F" : "M");
   printf("%s 1 %s 0.0 %f <O,%s> %s\n", wav, spk, durH[wav], gender, ref);
  }
  ' $dataset/phn_text > $dataset/stm || exit 1
  echo "stm file complete!"

  # generate glm file
  echo ';; empty.glm
  [FAKE]     =>  %HESITATION     / [ ] __ [ ] ;; hesitation token
  ' > $dataset/glm
  echo "glm file complete!"

  # decoding
  steps/nnet/decode.sh --nj $decode_nj --cmd "$decode_cmd" --acwt 0.2 \
      exp/mono/graph $dataset exp/dnn4_pretrain-dbn_dnn/decode_dev || exit 1

  echo "Decoding complete! The result is in 'exp/dnn4_pretrain-dbn_dnn/decode_dev/score*' !"
fi
exit 0
if [ $stage -le 5 ]; then
  echo ============================================================================
  echo "                      Generate Posteriorgrams                            "
  echo ============================================================================
  feature_transform=exp/dnn4_pretrain-dbn_dnn/final.feature_transform
  nnet=exp/dnn4_pretrain-dbn_dnn/final.nnet
  # dev set
  feats="ark,s,cs:copy-feats scp:co-feats/combine_features/com_mfcc_dev.scp ark:- |"
  nnet-forward --feature-transform=$feature_transform --use-gpu=yes "$nnet" "$feats" ark,t:data/dev/dev_post-softmax.txt || exit 1
  # test set
  feats="ark,s,cs:copy-feats scp:co-feats/combine_features/com_mfcc_test.scp ark:- |"
  nnet-forward --feature-transform=$feature_transform --use-gpu=yes "$nnet" "$feats" ark,t:data/test/test_post-softmax.txt || exit 1

  echo "Posteriorgrams Done! The result files are data/dev/dev_post-softmax.txt and data/test/test_post-softmax.txt"
fi

echo Finish
exit 0
