#!/bin/bash

copies=20
dir=co-feats

[ ! -d posteriorgrams ] && mkdir posteriorgrams

echo "Split timit and chloe data!"
for x in dev test; do
	python analysis_code/together.py $dir/$x $x $copies || exit 1
	python analysis_code/split_timit_chloe.py data/$x $x posteriorgrams $dir/$x || exit 1
done

[ ! -d posteriorgrams/timit ] && mkdir posteriorgrams/timit
[ ! -d posteriorgrams/chloe ] && mkdir posteriorgrams/chloe

echo "combine test and dev set!"
python analysis_code/combine_dev_test.py posteriorgrams || exit 1

echo "shuffle list!"
utils/shuffle_list.pl --srand 777 <posteriorgrams/chloe/ali_td_phones_tmp.txt >posteriorgrams/chloe/ali_td_phones.txt || exit 1

echo "create frame id!"
python analysis_code/balenced_frameid2num.py 3000 2 posteriorgrams/chloe || exit 1

echo "create posteriorgrams from frameid!"
python analysis_code/posteriorgrams_from_frameid.py posteriorgrams/chloe || exit 1

python analysis_code/change144_48.py sum balenced posteriorgrams/chloe
