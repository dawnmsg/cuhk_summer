#
# python analysis_code/balenced_frameid2num.py num_points_perphn num_frame_discard rdir
import sys
import random

def judge_finished(phn_frame_tmp):
	finished = True
	for i in range(48):
		if i+1 not in non_phn_list and len(phn_frame_tmp[i]) < num_points_perphn:
			finished = False

	return finished

num_points_perphn = int(sys.argv[1])
num_frame_discard = int(sys.argv[2])

min_copies = 2*num_frame_discard+1

rdir = sys.argv[3]+"/"
wdir = sys.argv[3]+"/"
non_phn_list = [1,11,14,18,44]
phn_frame = [[] for i in range(48)]
FC = 0
MC = 0
FM = 0
MM = 0
num_sentence = 0

rfile = open(rdir+"ali_td_phones.txt","r")
for line in rfile.readlines():
	judge = judge_finished(phn_frame)
	if judge == True:
		break
	frame = 0
	sentenceid = line.split(" ")[0]
	if sentenceid[:2] == "FC":
		FC = FC+1
	if sentenceid[:2] == "MC":
		MC = MC+1
	if sentenceid[:2] == "FM":
		FM = FM+1
	if sentenceid[:2] == "MM":
		MM = MM+1
	num_sentence = num_sentence+1
	txt = line.split(";")
	for num_pair in txt:
		phnid, copies = num_pair.split(" ")[1:3]
		if phnid != "1":
			if int(copies) > min_copies and len(phn_frame[int(phnid)-1]) < num_points_perphn:
				for j in range(num_frame_discard, int(copies)-num_frame_discard):
					frameid = sentenceid+"_"+str(frame+j)
					phn_frame[int(phnid)-1].append(frameid)

		frame = frame+int(copies)
rfile.close()

print("Total Sentences: "+str(num_sentence)+" \n")
print("FC Sentences: "+str(FC)+" "+str(FC/num_sentence)+" \n")
print("MC Sentences: "+str(MC)+" "+str(MC/num_sentence)+" \n")
print("FM Sentences: "+str(FM)+" "+str(FM/num_sentence)+" \n")
print("MM Sentences: "+str(MM)+" "+str(MM/num_sentence)+" \n")
# for i in range(48):
# 	if i+1 not in non_phn_list:
# 		print(len(phn_frame[i]))

wfile = open(wdir+"balenced_frameid2num_tr.txt","w")
frame = 0
for j in range(3):
	for i in range(48):
		if i+1 not in non_phn_list:
			wfile.write(phn_frame[i][j]+" "+str(frame)+" \n")
			frame = frame+1

for i in range(48):
	if i+1 not in non_phn_list:
		for j in range(3,num_points_perphn):
			wfile.write(phn_frame[i][j]+" "+str(frame)+" \n")
			frame = frame+1

wfile.close()
