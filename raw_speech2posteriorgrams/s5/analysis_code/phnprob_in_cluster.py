#
# python phnprob_in_cluster.py Clusters /home/leo/result_kmeans_60_unsil dataset
import os
import sys

def normalize(data_array):
	data_sum = 0
	num = len(data_array)
	for i in range(num):
		data_sum = data_sum+data_array[i]
	if data_sum == 0:
		return -1
	for i in range(num):
		data_array[i] = float(data_array[i])/data_sum
	return data_array

def sort(data_array):
	num = len(data_array)
	sub_array = []
	for i in range(num):
		for k in range(num):
			if k not in sub_array:
				dmax = k
				break
		for j in range(num):
			if((j not in sub_array)and(data_array[j]>data_array[dmax])):
				dmax = j
		sub_array.append(dmax)
	return sub_array

Clusters = int(sys.argv[1])
fdir = sys.argv[2]+"/"
dataset = sys.argv[3]

wfile1 = open(fdir+"phnprob_in_clusters.txt","w")
wfile2 = open(fdir+"phnidprob_in_clusters.txt","w")
rfile = open("/home/leo/mdd/data/"+dataset+"_frameid2ali-phone.txt","r")

framedict = {}
for line in rfile.readlines():
	key, value = line.split(" ")[:2]
	framedict.update({key:value})

rfile.close()

rfile = open("/home/leo/phones.txt","r")
phone_dict = {}
for line in rfile.readlines():
	value, key = line.split(" ")[:2]
	key = key[:-1]
	phone_dict.update({key:value})

rfile.close()

for i in range(Clusters):
	file_list = os.listdir(fdir+str(i))
	file_list.sort()
	phnprob = [0 for j in range(48)]
	for fl in file_list:
		txt = fl[:-4]
		if framedict.get(txt) == None:
			print(i)
			print(txt)
			exit()
		phone = int(framedict.get(txt))-1
		phnprob[phone] = phnprob[phone]+1

	phnprob = normalize(phnprob)
	if phnprob == -1:
		print(fdir+str(i)+" have no data!")
		wfile1.write("Cluster "+str(i)+" None of data! \n")
		wfile2.write("Cluster "+str(i)+" None of data! \n")
		continue
	sub_array = sort(phnprob)
	wfile1.write("Cluster "+str(i)+" ")
	wfile2.write("Cluster "+str(i)+" ")
	for sub in sub_array:
		if phnprob[sub] > 0:
			wfile1.write(phone_dict.get(str(sub+1))+": "+str(phnprob[sub])+"; ")
			wfile2.write(str(sub+1)+": "+str(phnprob[sub])+"; ")
	wfile1.write("\n")
	wfile2.write("\n")

wfile1.close()
wfile2.close()



