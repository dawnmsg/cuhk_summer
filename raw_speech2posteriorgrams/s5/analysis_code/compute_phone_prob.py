#
# python compute_phone_prob.py dataset
import sys

def normalize(data_array):
	num = len(data_array)
	tmp_array = [0 for i in range(num)]
	data_sum = 0
	for i in range(num):
		tmp_array[i] = data_array[i]
		data_sum = data_sum+tmp_array[i]
	for i in range(num):
		tmp_array[i] = float(tmp_array[i])/data_sum
	return tmp_array

dataset = sys.argv[1]
rdir = "/home/leo/mdd/data/"
rfilename = sys.argv[1]+"_frameid2ali-phone.txt"
wfilename = sys.argv[1]+"_phn_prob.txt"

rfile = open(rdir+rfilename,"r")
wfile = open(rdir+wfilename,"w")

referfile = open("/home/leo/phones.txt","r")
phone_dict = {}
for line in referfile.readlines():
	value, key = line.split(" ")
	key = key[:-1]
	phone_dict.update({key:value})

referfile.close()

phone_array = [0 for i in range(48)]

for line in rfile.readlines():
	phone = int(line.split(" ")[1])
	phone_array[phone-1] = phone_array[phone-1]+1

rfile.close()

nor_phn = normalize(phone_array)

for i in range(48):
	wfile.write(str(i+1)+" "+phone_dict.get(str(i+1))+" : "+str(phone_array[i])+" "+str(nor_phn[i])+" \n")

wfile.close()
