#
# python analysis_code/phn_distribution_in_clusters.py Clusters num_state /home/leo/result_kmeans_60_unsil_timit_test_48 dataset
import sys

def normalize(data_array):
	data_sum = 0
	num = len(data_array)
	tmp = [0 for i in range(num)]
	for i in range(num):
		tmp[i] = int(data_array[i])
		data_sum = data_sum+tmp[i]
	if data_sum == 0:
		return -1
	for i in range(num):
		tmp[i] = float(tmp[i])/data_sum
	return tmp

Clusters = int(sys.argv[1])
num_state = int(sys.argv[2])
wdir = sys.argv[3]+"/"
dataset = sys.argv[4]

if dataset == "timit":
	if num_state == 144:
		rstfile = open("/home/leo/mdd/result/timitresult/unsil_"+str(Clusters)+"_kmeans.csv","r")

	if num_state == 48:
		rstfile = open("/home/leo/mdd/result/timitresult/unsil_"+str(Clusters)+"_kmeans_"+str(num_state)+".csv","r")

if dataset == "cu-chloe":
	if num_state == 144:
		rstfile = open("/home/leo/mdd/result/clusterresult/kmeans_"+str(Clusters)+"_unsil.csv","r")

	if num_state == 48:
		rstfile = open("/home/leo/mdd/result/clusterresult/kmeans_"+str(Clusters)+"_unsil_"+str(num_state)+".csv","r")

wfile = open(wdir+"phn_distribution.txt","w")
f2afile = open("/home/leo/mdd/data/"+dataset+"_frameid2ali-phone.txt","r")
f2nfile = open("/home/leo/mdd/data/"+dataset+"_frameid2num_unsil.txt","r")
phonefile = open("/home/leo/phones.txt","r")

frameid2num_dict = {}
for line in f2nfile.readlines():
	value, key = line.split(" ")[:2]
	frameid2num_dict.update({key:value})

f2nfile.close()

frameid2phn_dict = {}
for line in f2afile.readlines():
	key, value = line.split(" ")[:2]
	frameid2phn_dict.update({key:value})

f2afile.close()

phn_dict = {}
for line in phonefile.readlines():
	value, key = line.split(" ")
	key = key[:-1]
	phn_dict.update({key:value})

phonefile.close()


phn_list = [[0 for i in range(Clusters)] for j in range(48)]

for line in rstfile.readlines():
	fnum, cluster = line.split(",")
	cluster = cluster[:-1]
	fid = frameid2num_dict.get(fnum)
	phnid = frameid2phn_dict.get(fid)
	phn_list[int(phnid)-1][int(cluster)] = phn_list[int(phnid)-1][int(cluster)]+1

rstfile.close()

for i in range(48):
	phn_distribution = normalize(phn_list[i])
	phn = phn_dict.get(str(i+1))
	wfile.write(str(i+1)+" "+phn+": ")
	if phn_distribution == -1:
		wfile.write("None \n")
		continue
	non_zero_cluster = []
	for j in range(Clusters):
		if phn_distribution[j] > 0:
			non_zero_cluster.append(j)

	for cluster in non_zero_cluster[:-1]:
		wfile.write(str(cluster)+" "+str(phn_distribution[cluster])+"; ")
	cluster = non_zero_cluster[-1]
	wfile.write(str(cluster)+" "+str(phn_distribution[cluster])+"; \n")

wfile.close()
