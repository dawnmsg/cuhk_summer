#
# python analysis_code/posteriorgrams_from_frameid.py posteriorgrams/chloe
import sys

rdir = sys.argv[1]+"/"

sentenceid_line_dict = {}
i = 0
rfile = open(rdir+"td_post-softmax.txt","r")
for line in rfile.readlines():
	if line[0] != " ":
		sentenceid = line.split(" ")[0]
		sentenceid_line_dict.update({sentenceid:i})
	i = i+1 

rfile.close()

print("Dict Prepare succeed!")
rfile1 = open(rdir+"td_post-softmax.txt","r")
rfile2 = open(rdir+"balenced_frameid2num_tr.txt","r")
wfile = open(rdir+"144.balenced_posteriorgrams_tr.csv","w")

lines = rfile1.readlines()

for line in rfile2.readlines():
	frameid = line.split(" ")[0]
	people, sentence, frame = frameid.split("_")
	sentenceid = people+"_"+sentence
	num_line = sentenceid_line_dict.get(sentenceid)+int(frame)+1
	txt = lines[num_line].split(" ")
	for prob in txt[2:-2]:
		wfile.write(prob+",")
	prob = txt[-2]
	wfile.write(prob+"\n")

rfile1.close()
rfile2.close()
wfile.close()
