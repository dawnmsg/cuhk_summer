#
# python analysis-code/wavf2clusters.py /home/leo/result_kmeans_60_unsil 60 newframe2num.txt result_kmeans_60_unsil.txt
import sys
import os

dataset = sys.argv[5]
fdir = "wave_split_"+dataset+"/"
destinationpath = sys.argv[1]+"/"
clusters = int(sys.argv[2])
referfilename = sys.argv[3]
rfilename = sys.argv[4]

os.system("mkdir -p "+destinationpath[:-1])
for i in range(clusters):
	os.system("mkdir "+destinationpath+str(i))

frame_dict = {}
referfile = open(referfilename,"r")
for line in referfile.readlines():
	value, key = line.split(" ")[:2]
	frame_dict.update({key:value})

referfile.close()

rfile = open(rfilename,"r")
for line in rfile.readlines():
	frame_num, cluster = line.split(",")
	cluster = cluster[:-1]
	frame_id = frame_dict.get(frame_num)
	person, sentenceid, frame = frame_id.split("_")
	command = "cp "+fdir+person+"/"+sentenceid+"/"+frame_id+".wav "+destinationpath+cluster+"/"
	os.system(command)

rfile.close()
