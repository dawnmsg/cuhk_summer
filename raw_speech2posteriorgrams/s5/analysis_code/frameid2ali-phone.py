#
# python analysis_code/frameid2ali-phone.py dataset
import sys
dataset = sys.argv[1]

if dataset == "timit":
	fdir = "/home/leo/kaldi-master/egs/timit/s5/co-feats/"
	data_copies = 10
	wfile = open("/home/leo/mdd/data/timit_frameid2ali-phone.txt","w")
	dset = "dev"
elif dataset == "cu-chloe":
	fdir = "/home/leo/kaldi-master/egs/cu-chloe_tmp/s5/co-feats/"
	data_copies = 2
	wfile = open("/home/leo/mdd/data/cu-chloe_frameid2ali-phone.txt","w")
	dset = "test"
elif dataset == "all_cu-chloe":
	fdir = "/home/leo/all_cu-chloe/co-feats/"
	data_copies = 10
	wfile = open("/home/leo/mdd/data/all_cu-chloe_frameid2ali-phone.txt","w")
	dset = "test"
else:
	print("Wrong dataset!")
	exit()

for i in range(data_copies):
	rfile = open(fdir+"ali_"+dset+"_phones."+str(i+1)+".txt","r")
	for line in rfile.readlines():
		txt = line.split(";")
		sentenceid = txt[0].split(" ")[0]
		frame = 0
		for num_pair in txt:
			phone = num_pair.split(" ")[1]
			num = int(num_pair.split(" ")[2])
			for j in range(num):
				wfile.write(sentenceid+"_"+str(frame)+" "+phone+" \n")
				frame = frame+1

	rfile.close()
wfile.close()