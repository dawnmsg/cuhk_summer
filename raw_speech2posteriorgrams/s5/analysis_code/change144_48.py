#
# python analysis_code/change144_48.py stype: sum_middle dataset rdir
import sys

stype = sys.argv[1]
dataset = sys.argv[2]
rdir = sys.argv[3]+"/"

rfile = open(rdir+"144."+dataset+"_posteriorgrams_tr.csv","r")
if stype == "sum":
	wfile = open("/home/leo/mdd/data/48."+dataset+"_posteriorgrams_tr.csv","w")
	for line in rfile.readlines():
		txt = line.split(',')
		txt[-1] = txt[-1][:-1]
		i = 0
		total = 0
		for num in txt[:-3]:
			total = total+float(num)
			if(i%3==2):
				wfile.write(str(total)+",")
				total = 0
			i = i+1
		total = float(txt[-3])+float(txt[-2])+float(txt[-1])
		wfile.write(str(total)+"\n")
	wfile.close()
elif stype == "middle":
	wfile = open("/home/leo/mdd/data/48."+dataset+"_posteriorgrams.csv","w")
	for line in rfile.readlines():
		txt = line.split(',')
		txt[-1] = txt[-1][:-1]
		i = 0
		phn_prob = []
		for num in txt[:-3]:
			if(i%3==1):
				phn_prob.append(float(num))
			i = i+1
		phn_prob.append(float(txt[-2]))
		prob_sum = sum(phn_prob)
		for prob in phn_prob[:-1]:
			# prob_normal = prob/prob_sum
			prob_normal = prob
			wfile.write(str(prob_normal)+",")
		# prob_normal = phn_prob[-1]/prob_sum
		prob_normal = phn_prob[-1]
		wfile.write(str(prob_normal)+"\n")
	wfile.close()
else:
	print("Wrong Stype!")

rfile.close()

