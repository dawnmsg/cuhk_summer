#
# python analysis_code/chloe_train_ltext.py /home/leo/Transcription
import sys
import os

fdir = sys.argv[1]+"/"
wfile = open("/home/leo/chloe_ltext_train.txt","w")

rfile = open("/home/leo/kaldi-master/egs/timit/s5/conf/phones.60-48-39.map","r")
phone_dict = {}
phn48_list = []
non_phn = []
for line in rfile.readlines():
	phn_list = line.split("	")
	phn_list[-1] = phn_list[-1][:-1]
	if len(phn_list) < 2:
		continue
	phn60, phn48 = line.split("	")[:2]
	phn48_list.append(phn48)
	phone_dict.update({phn60:phn48})

rfile.close()

train_list = ["FC001","FC002","MC001","MC002","FM001","FM002","MM001"]

cate_list = os.listdir(fdir)
cate_list.sort()

for cate in cate_list:
	people_list = os.listdir(fdir+cate+"/")
	people_list.sort()
	for people in people_list:
		if people in train_list:
			sentence_list = os.listdir(fdir+cate+"/"+people+"/")
			sentence_list.sort()
			for sentence in sentence_list:
				if sentence[-4:] == ".lab":
					rfile = open(fdir+cate+"/"+people+"/"+sentence,"r")
					wfile.write(people+"_"+sentence[:-4]+" ")
					for line in rfile.readlines():
						phoneme = line.split(" ")[2]
						if phoneme == "sp":
							phoneme = "sil"
						if phoneme not in phn48_list:
							phn48 = phone_dict.get(phoneme)
							if phn48 == None:
								if phoneme not in non_phn:
									non_phn.append(phoneme)
						wfile.write(phoneme+" ")
					wfile.write("\n")
					rfile.close()

wfile.close()

for phn in non_phn:
	print(phn+"\n")
