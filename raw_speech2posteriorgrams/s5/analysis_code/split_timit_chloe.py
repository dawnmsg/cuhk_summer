#
# python local/split_timit_chloe.py $test_dir test $posteriorgrams $co-feats/test
import sys

rdir = sys.argv[1]+"/"
dataset = sys.argv[2]
posteriorgrams_dir = sys.argv[3]+"/"

rfile = open(rdir+dataset+"_post-softmax.txt","r")
timit_wfile = open(posteriorgrams_dir+"timit_"+dataset+"_post-softmax.txt","w")

print("posteriorgrams")
print("timit")
dset = "timit"
for line in rfile.readlines():
	if line[0] != " ":
		if line[2] == "0":
			dset = "chloe"
		else:
			dset = "timit"
			timit_wfile.write(line)
	else:
		if dset == "timit":
			timit_wfile.write(line)

rfile.close()
timit_wfile.close()

rfile = open(rdir+dataset+"_post-softmax.txt","r")
chloe_wfile = open(posteriorgrams_dir+"chloe_"+dataset+"_post-softmax.txt","w")

print("chloe")
for line in rfile.readlines():
	if line[0] != " ":
		if line[2] == "0":
			dset = "chloe"
			chloe_wfile.write(line)
		else:
			dset = "timit"
	else:
		if dset == "chloe":
			chloe_wfile.write(line)

rfile.close()
chloe_wfile.close()

rdir = sys.argv[4]+"/"
rfile = open(rdir+"ali_"+dataset+"_phones.txt","r")
timit_wfile = open(posteriorgrams_dir+"timit_"+dataset+"_aliphones.txt","w")

print("aliphones")
print("timit")
for line in rfile.readlines():
	if line[2] != "0":
		timit_wfile.write(line)

rfile.close()
timit_wfile.close()

rfile = open(rdir+"ali_"+dataset+"_phones.txt","r")
chloe_wfile = open(posteriorgrams_dir+"chloe_"+dataset+"_aliphones.txt","w")

print("chloe")
for line in rfile.readlines():
	if line[2] == "0":
		chloe_wfile.write(line)

rfile.close()
chloe_wfile.close()
