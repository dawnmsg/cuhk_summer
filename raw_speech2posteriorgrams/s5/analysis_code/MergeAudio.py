#
import os
import wave
import sys

clusters = int(sys.argv[1])
fdir = sys.argv[2]+"/"

for i in range(clusters):
	samples = 0
	wave_data = []
	files = os.listdir(fdir+str(i))
	if len(files) == 0:
		continue
	wfile = wave.open(fdir+str(i)+".wav","wb")
	files.sort()
	for wav_file in files:
		rfile = wave.open(fdir+str(i)+"/"+wav_file,"rb")
		nchannels, sampwidth, framerate, nframes, comptype, compname = rfile.getparams()[:6]
		if samples == 0:
			wfile.setnchannels(nchannels)
			wfile.setsampwidth(sampwidth)
			wfile.setframerate(framerate)
			wfile.setcomptype(comptype, compname)
		wave_data.append(rfile.readframes(nframes))
		samples = samples+nframes
		rfile.close()
	wfile.setnframes(samples)
	for data in wave_data:
		wfile.writeframes(data)
	wfile.close()
	print("Done "+fdir+str(i)+".wav!\n")

