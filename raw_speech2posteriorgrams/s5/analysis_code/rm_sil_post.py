#
rdir = "/home/leo/mdd/data/"
rfile = open(rdir+"timit_unsilence-frameid.txt","r")
unsil_frame_dict = {}

for line in rfile.readlines():
	key, value = line.split(" ")[:2]
	unsil_frame_dict.update({key:value})

rfile.close()

rfile = open(rdir+"dev_post-softmax.txt","r")
wfile = open(rdir+"dev_post-softmax_unsil.txt","w")

for line in rfile.readlines():
	if line[0] != " ":
		sentenceid = line.split(" ")[0]
		frame = -1
		wfile.write(line)
	else:
		if unsil_frame_dict.get(sentenceid+"_"+str(frame)) != None:
			wfile.write(line)

	frame = frame+1

rfile.close()
wfile.close()

