#this file is used to generate posteriorgrams from "test_post-softmax.txt"
import sys

num_state = int(sys.argv[1])
rdir = sys.argv[2]+"/"
dataset = sys.argv[3]
rfile = open(rdir+dataset+"_post-softmax_unsil.txt","r")
wfile = open(rdir+str(num_state)+"."+dataset+"_posteriorgrams.txt","w")

if(num_state==144):
	for line in rfile.readlines():
		if(line[0]==' '):
			if(line[-2]==']'):
				wfile.write(line[2:-3]+" \n")
			else:
				wfile.write(line[2:-2]+" \n")
elif(num_state==48):
	for line in rfile.readlines():
		if(line[0]==' '):
			txt = line.split(' ')[2:-1]
			i = 0
			total = 0
			for num in txt:
				total = total+float(num)
				if(i%3==2):
					wfile.write(str(total)+" ")
					total = 0
				i = i+1
			wfile.write("\n")
else:
	print("Wrong number of states!\n")

rfile.close()
wfile.close()
