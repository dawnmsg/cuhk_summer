#
import os
fdir = "/home/leo/kaldi-master/egs/timit/s5/data/dev/"
desdir = "/home/leo/timit_dev/"
rfile = open(fdir+"wav.scp","r")

for line in rfile.readlines():
	index = line.find(" ")
	wavid = line[:index]
	people, sentence = wavid.split("_")
	os.system("[ ! -d "+desdir+people+" ] && mkdir -p "+desdir+people)
	command = line[index+1:-3]
	destination = command.split(" ")[-1]
	os.system("cp "+destination[:-3]+"* "+desdir+people)
	os.system(command+" "+desdir+people+"/"+sentence+".wav")

rfile.close()
