#!/bin/bash

#this file is used to generate posteriorgrams
dir=/home/leo/mdd/data
num_state=144
dataset=timit
echo "$0 $@"
echo "num_state="$num_state
python ./analysis_code/gen-posteriorgrams.py $num_state $dir $dataset

# utils/shuffle_list.pl --srand 777 <$num_state.test_posteriorgrams.txt >$num_state.test_posteriorgrams_shuffled.txt

python ./analysis_code/change_format.py $num_state $dir $dataset
