#
# python analysis_code/frame_md_acc.py /home/leo/balance_100_resultslabelsstd.txt
import sys
rfilename = sys.argv[1]

rfile = open(rfilename,"r")
wfile = open(rfilename[:-4]+"_rst.txt","w")

TA = 0
TR = 0
FA = 0
FR = 0
s_r = 0
l_r = 0
s_l = 0
frame = 0

rfile.readline()

for line in rfile.readlines():
	frame = frame+1
	std, label, result = line.split(" ")[2:5]
	result = result[:-1]
	if std == label:
		s_l = s_l+1
		if std == result:
			TA = TA+1
			s_r = s_r+1
		else:
			FR = FR+1
	else:
		if std == result:
			s_r = s_r+1
			FA = FA+1
		else:
			TR = TR+1
	if label == result:
		l_r = l_r+1

rfile.close()

acc = (TA+TR)/(TA+TR+FR+FA)

wfile.write("TA = "+str(TA)+" \n")
wfile.write("TR = "+str(TR)+" \n")
wfile.write("FA = "+str(FA)+" \n")
wfile.write("FR = "+str(FR)+" \n")
wfile.write("s_l_similar = "+str(s_l/frame)+" \n")
wfile.write("s_r_similar = "+str(s_r/frame)+" \n")
wfile.write("l_r_similar = "+str(l_r/frame)+" \n")
wfile.write("FRR = "+str(FR/(TA+FR))+" \n")
wfile.write("FAR = "+str(FA/(FA+TR))+" \n")
wfile.write("Precision = "+str(TR/(TR+FR))+" \n")
wfile.write("Recall = "+str(TR/(TR+FA))+" \n")
wfile.write("ACC = "+str(acc)+" \n")
wfile.close()
