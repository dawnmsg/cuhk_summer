#
# python analysis_code/analysis_cer.py /home/leo/mdd/result/clusterresult kmeans_60_cer.csv
import sys

def analysis_data(data_list):
	num = len(data_list)
	state = 0
	for i in range(num):
		if data_list[i] > data_list[state]:
			state = i
	if (state+1)%3 != 0:
		phone = int((state+1)/3)+1
	else:
		phone = int((state+1)/3)

	return state, phone, data_list[state]

fdir = sys.argv[1]+"/"
filename = sys.argv[2]

rfile = open(fdir+filename,"r")
wfile = open(fdir+filename[:-4]+".analysis"+filename[-4:],"w")

Cluster = 0
for line in rfile.readlines():
	data_list = []
	for num in line.split(","):
		if num[-1] == "\n":
			num = num[:-1]
		data_list.append(float(num))
	state, phone, prob = analysis_data(data_list)
	phn_state = (state+1)%3-1
	if phn_state < 0:
		phn_state = phn_state+3 
	wfile.write("Cluster "+str(Cluster)+" "+str(state)+" "+str(phone)+"-"+str(phn_state)+" "+str(prob)+" \n")
	Cluster = Cluster+1

rfile.close()
wfile.close()