#
# python analysis_code/combine_dev_test.py $posteriorgrams
import sys
posteriorgrams_dir = sys.argv[1]+"/"
timit_dir = posteriorgrams_dir+"timit/"
chloe_dir = posteriorgrams_dir+"chloe/"

# timit part
test_rfile = open(posteriorgrams_dir+"timit_test_aliphones.txt","r")
dev_rfile = open(posteriorgrams_dir+"timit_dev_aliphones.txt","r")
wfile = open(timit_dir+"ali_td_phones.txt","w")

for line in test_rfile.readlines():
	wfile.write(line)

for line in dev_rfile.readlines():
	wfile.write(line)

wfile.close()
test_rfile.close()
dev_rfile.close()

test_rfile = open(posteriorgrams_dir+"timit_test_post-softmax.txt","r")
dev_rfile = open(posteriorgrams_dir+"timit_dev_post-softmax.txt","r")
wfile = open(timit_dir+"td_post-softmax.txt","w")

for line in test_rfile.readlines():
	wfile.write(line)

for line in dev_rfile.readlines():
	wfile.write(line)

wfile.close()
test_rfile.close()
dev_rfile.close()

# chloe part
test_rfile = open(posteriorgrams_dir+"chloe_test_aliphones.txt","r")
dev_rfile = open(posteriorgrams_dir+"chloe_dev_aliphones.txt","r")
wfile = open(chloe_dir+"ali_td_phones_tmp.txt","w")

for line in test_rfile.readlines():
	wfile.write(line)

for line in dev_rfile.readlines():
	wfile.write(line)

wfile.close()
test_rfile.close()
dev_rfile.close()

test_rfile = open(posteriorgrams_dir+"chloe_test_post-softmax.txt","r")
dev_rfile = open(posteriorgrams_dir+"chloe_dev_post-softmax.txt","r")
wfile = open(chloe_dir+"td_post-softmax.txt","w")

for line in test_rfile.readlines():
	wfile.write(line)

for line in dev_rfile.readlines():
	wfile.write(line)

wfile.close()
test_rfile.close()
dev_rfile.close()
