
rdir = "/home/leo/kaldi-master/egs/timit/s5/co-feats/"
data_copies = 10
wdir = "/home/leo/mdd/data/"
wfile = open(wdir+"timit_frameid2num_unsil.txt","w")

unsil_frame = 0
for i in range(data_copies):
	rfile = open(rdir+"ali_dev_phones."+str(i+1)+".txt","r")
	for line in rfile.readlines():
		txt = line.split(";")
		sentenceid = txt[0].split(" ")[0]
		num_frame = 0
		for num_pair in txt:
			a = int(num_pair.split(" ")[1])
			b = int(num_pair.split(" ")[2])
			if a == 1:
				num_frame = num_frame+b
			else:
				for j in range(b):
					wfile.write(sentenceid+"_"+str(num_frame)+" "+str(unsil_frame)+" \n")
					num_frame = num_frame+1
					unsil_frame = unsil_frame+1

	rfile.close()

wfile.close()
