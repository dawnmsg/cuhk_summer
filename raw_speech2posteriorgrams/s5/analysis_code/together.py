# this file copies all the scps into one file
# python together.py co-feats/train train total_copies
import sys
ldir = sys.argv[1]+"/"
dataset = sys.argv[2]
total_copies = int(sys.argv[3])

wfile = open(ldir+"ali_"+dataset+"_phones.txt","w")
for i in range(total_copies):
	rfile = open(ldir+"ali_"+dataset+"_phones."+str(i+1)+".txt","r")
	for line in rfile.readlines():
		wfile.write(line)

	rfile.close()

wfile.close()
