#this file is used to split wav file 
import wave
import sys
import os

fdir = "/home/leo/wave_split_"+sys.argv[3]+"/"+sys.argv[1][-6:]
infile = sys.argv[2]
frame_L = 25
frame_S = 10
frame_C = 5

rfile = wave.open(sys.argv[1]+infile,"rb")
os.system("if [ ! -d "+fdir[:]+infile[:-4]+" ]; then mkdir -p "+fdir[:]+infile[:-4]+"; fi")

nchannels, sampwidth, samplerate, nsamples, comptype, compname = rfile.getparams()[:6]

print("Original file info:\n")
print("channel ",nchannels)  
print("sample_width ",sampwidth)  
print("samplerate ",samplerate)  
print("numsamples ",nsamples)
print("\n\n")

flist = []
fL = int(frame_L*samplerate/1000)
fS = int(frame_S*samplerate/1000)

for i in range(nsamples):
	flist.append(rfile.readframes(1))

nframes = int((nsamples-fL)/fS)+1
normal_L = fL+frame_C*2*fS
thred_l = frame_C
thred_h = int((nsamples-fL)/fS)-frame_C

for frame in range(nframes):
	filename = fdir[:]+infile[:-4]+"/"+fdir[-6:-1]+"_"+infile[:-4]+"_"+str(frame)+".wav"
	if(os.path.isfile(filename)):
		print("file: "+filename+" has already been splitted, skipping\n")
	else:
		wfile = wave.open(filename,"wb")
		wfile.setnchannels(nchannels)
		wfile.setsampwidth(sampwidth)
		wfile.setframerate(samplerate)
		wfile.setcomptype(comptype, compname)
		if(frame<thred_l):
			wfile.setnframes((frame+frame_C)*fS+fL)
			for i in range((frame+frame_C)*fS+fL):
				wfile.writeframes(flist[i])
		elif(frame>thred_h):
			wfile.setnframes(nsamples-(frame-frame_C)*fS)
			for i in range((frame-frame_C)*fS,nsamples):
				wfile.writeframes(flist[i])
		else:
			wfile.setnframes(normal_L)
			for i in range((frame-frame_C)*fS,(frame+frame_C)*fS+fL):
				wfile.writeframes(flist[i])

		wfile.close()

rfile.close()



