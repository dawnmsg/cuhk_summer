#
# python analysis_code/clusters_entropy.py /home/leo/mdd/result/clusterresult 60 num_state
import sys
import math

def compute_H(data_distribution):
	H = 0
	for prob in data_distribution:
		prob = float(prob)
		if prob > 0:
			H = H-float(prob)*math.log(float(prob))

	return H

rdir = sys.argv[1]+"/"
clusters = int(sys.argv[2])
num_state = int(sys.argv[3])
if num_state == 144:
	rfilename = "kmeans_"+str(clusters)+"_cer.csv"
if num_state == 48:
	rfilename = "kmeans_"+str(clusters)+"_cer_"+str(num_state)+".csv"
rfile = open(rdir+rfilename,"r")
wfile = open(rdir+rfilename[:-4]+"_entropy.txt","w")

cluster = 0
for line in rfile.readlines():
	prob = line.split(",")
	prob[-1] = prob[-1][:-1]
	entropy = compute_H(prob)
	wfile.write("Cluster "+str(cluster)+" "+str(entropy)+" \n")
	cluster = cluster+1

rfile.close()
wfile.close()