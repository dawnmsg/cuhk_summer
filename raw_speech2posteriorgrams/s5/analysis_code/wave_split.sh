#!/bin/bash
#this file is used for splitting wave files 
dataset=timit

if [ $dataset == "cu-chloe" ]; then
	dir=/home/leo/CU-CHLOE/Recordings
	for cate in $dir/*
	do
		for people in $cate/*
		do
			for file in $people/*.wav
			do
				python analysis_code/wave_split.py $people/ ${file##*/} $dataset
			done
		done
	done
fi

if [ $dataset == "timit" ]; then
	dir=/home/leo/timit_dev
	for people in $dir/*
	do
		for file in $people/*.wav
		do
			python analysis_code/wave_split.py $people/ ${file##*/} $dataset
		done
	done
fi