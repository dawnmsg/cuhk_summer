#
# python analysis_code/label_cluster.py /home/leo/result_kmeans_70_unsil_timit_test_48 clusters
import sys

rdir = sys.argv[1]+"/"
clusters = int(sys.argv[2])

phn_probfile = open(rdir+"phnprob_in_clusters.txt","r")
cluster_probfile = open(rdir+"phn_distribution.txt","r")
wfile = open(rdir+"label_clusters.txt","w")

corpron_dict = {}
mispron_non_list = []
mispron_similar_list = [[] for i in range(clusters)]
mixcluster_list = [[] for i in range(clusters)]

phn2cluster_dict = {}

for line in cluster_probfile.readlines():
	index = line.find(":")
	phone = line[:index].split(" ")[1]
	if phone == "sil":
		continue
	txt = line[index+1:].split(";")[:-1]
	for item in txt:
		cluster, prob = item.split(" ")[1:3]
		if float(prob) > 0.95:
			phn2cluster_dict.update({phone:cluster})
			break

cluster_probfile.close()

for line in phn_probfile.readlines():
	index = line.find(" ")
	line = line[index+1:]
	index = line.find(" ")
	cluster = line[:index]
	if line[index+1:-2] == "None of data!":
		mispron_non_list.append(cluster)
		continue
	txt = line[index:].split(";")[:-1]
	fphone, fprob = txt[0].split(" ")[1:3]
	fphone = fphone[:-1]
	if float(fprob) > 0.95:
		if phn2cluster_dict.get(fphone) == cluster:
			mix = False
			for item in txt[1:]:
				phone = item.split(" ")[1][:-1]
				if phn2cluster_dict.get(phone) == cluster:
					mixcluster_list[int(cluster)].append(phone)
					mix = True
			if mix == False:
				corpron_dict.update({cluster:fphone})
			else:
				mixcluster_list[int(cluster)].append(fphone)
		else:
			mispron_similar_list[int(cluster)].append(fphone)
	else:
		mix = False
		for item in txt:
			phone = item.split(" ")[1][:-1]
			if phn2cluster_dict.get(phone) == cluster:
				mixcluster_list[int(cluster)].append(phone)
				mix = True
		if mix == False:
			for item in txt:
				phone, prob = item.split(" ")[1:3]
				phone = phone[:-1]
				if float(prob) > 0.2:
					mispron_similar_list[int(cluster)].append(phone)

phn_probfile.close()

wfile.write("======================================================================\n")
wfile.write("                   Correct Pronunciation Clusters                     \n")
wfile.write("======================================================================\n")

for i in range(clusters):
	phone = corpron_dict.get(str(i))
	if phone != None:
		wfile.write("Cluster "+str(i)+" <===> "+phone+" \n")

wfile.write("======================================================================\n")
wfile.write(" Mispronunciation Clusters (unsimilar with any phone's pronunciation) \n")
wfile.write("======================================================================\n")

for cluster in mispron_non_list:
	wfile.write("Cluster "+cluster+" \n")

wfile.write("======================================================================\n")
wfile.write(" Mispronunciation Clusters (similar with some phone's pronunciation)  \n")
wfile.write("======================================================================\n")

for i in range(clusters):
	if len(mispron_similar_list[i]) != 0:
		wfile.write("Cluster "+str(i)+" ")
		for phone in mispron_similar_list[i]:
			wfile.write(phone+" ")
		wfile.write("\n")

wfile.write("======================================================================\n")
wfile.write("                     Mixed Pronunciation Clusters                     \n")
wfile.write("======================================================================\n")

for i in range(clusters):
	if len(mixcluster_list[i]) != 0:
		wfile.write("Cluster "+str(i)+" ")
		for phone in mixcluster_list[i]:
			wfile.write(phone+" ")
		wfile.write("\n")

wfile.close()
