#
# python analysis_code/wavsegment2cluster.py /home/leo/cluster_result_new/result_kmeans_100_unsil_48 cu-chloe 100
import sys
import os

rstdir = sys.argv[1]+"/"
dataset = sys.argv[2]
cluster = int(sys.argv[3])

wfile = open(rstdir+str(cluster)+"_"+dataset+"_result.csv","w")

for i in range(cluster):
	 wavfile = os.listdir(rstdir+str(i))
	 wavfile.sort()
	 for wave in wavfile:
	 	speakerid, sentenceid, frame = wave[:-4].split("_")
	 	segstart = int(frame)*10
	 	segend = segstart+25
	 	wfile.write(speakerid+","+sentenceid+","+str(segstart)+","+str(segend)+","+str(i)+"\n")

wfile.close()
