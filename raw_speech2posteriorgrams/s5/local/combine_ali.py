#this file is used to combine alis
import sys

binary_copies = int(sys.argv[1])
mfcc_copies = int(sys.argv[2])
qdict_dir = sys.argv[3]
dataset = sys.argv[4]
mfcc_dir = sys.argv[5]
com_dir = sys.argv[6]

wfile = open(qdict_dir+"/ali_"+dataset+"_binary.txt","w")
for i in range(binary_copies):
	rfile = open(qdict_dir+"/ali_"+dataset+"_binary."+str(i+1)+".txt","r")
	for line in rfile.readlines():
		wfile.write(line)

	rfile.close()

wfile.close()

rfile1 = open(qdict_dir+"/ali_"+dataset+"_binary.txt","r")
lines1 = rfile1.readlines()
j = 0
for i in range(mfcc_copies):
	rfile = open(mfcc_dir+"/raw_mfcc_"+dataset+"."+str(i+1)+".txt","r")
	wfile = open(com_dir+"/com_mfcc_"+dataset+"."+str(i+1)+".txt","w")
	for line in rfile.readlines():
		if(line[0]!=' '):
			wfile.write(line)
		elif(line[-2]==']'):
			wfile.write(line[:-2]+lines1[j][2:])
		else:
			wfile.write(line[:-1]+lines1[j][2:])

		j = j+1

	rfile.close()
	wfile.close()

rfile1.close()
