#!/bin/bash

. ./path.sh

# make_post
# labeldir=./exp/mono_ali
# dir=./co-feats
# for((i=1;i<31;i++));do	
#          ali-to-pdf $labeldir/final.mdl "ark:gunzip -c $labeldir/ali.$i.gz |" ark:- | ali-to-post ark:- ark,t:$dir/ali_post.$i.txt
# done

# make_label
labeldir=$1
dir=$2
data_copies=$3
dataset=$4
for((i=1;i<data_copies+1;i++));do
	gunzip -c $labeldir/ali.$i.gz | ali-to-phones --write-lengths=true exp/mono/final.mdl ark:- ark,t:$dir/ali_${dataset}_phones.$i.txt	
done
