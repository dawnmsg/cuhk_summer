#
# python local/combine_timit_cu-chloe.py $train_dir $test_dir $dev_dir
import sys

train_dir = sys.argv[1]+"/"
test_dir = sys.argv[2]+"/"
dev_dir = sys.argv[3]+"/"

rfile1 = open(train_dir+"timit_train_wav.scp","r")
rfile2 = open(train_dir+"cu-chloe_train_wav.scp","r")
wfile = open(train_dir+"wav.scp","w")

for line in rfile1.readlines():
	wfile.write(line)
for line in rfile2.readlines():
	wfile.write(line)

rfile1.close()
rfile2.close()
wfile.close()

rfile1 = open(train_dir+"timit_train.text","r")
rfile2 = open(train_dir+"cu-chloe_train.ltext","r")
wfile = open(train_dir+"text","w")

for line in rfile1.readlines():
	wfile.write(line)
for line in rfile2.readlines():
	wfile.write(line)

rfile1.close()
rfile2.close()
wfile.close()

rfile1 = open(train_dir+"timit_train.text","r")
rfile2 = open(train_dir+"cu-chloe_train.qtext","r")
wfile = open(train_dir+"qdict","w")

for line in rfile1.readlines():
	wfile.write(line)
for line in rfile2.readlines():
	wfile.write(line)

rfile1.close()
rfile2.close()
wfile.close()

rfile1 = open(test_dir+"timit_test_wav.scp","r")
rfile2 = open(test_dir+"cu-chloe_test_wav.scp","r")
wfile = open(test_dir+"wav.scp","w")

for line in rfile1.readlines():
	wfile.write(line)
for line in rfile2.readlines():
	wfile.write(line)

rfile1.close()
rfile2.close()
wfile.close()

rfile1 = open(test_dir+"timit_test.text","r")
rfile2 = open(test_dir+"cu-chloe_test.qtext","r")
wfile = open(test_dir+"qdict","w")

for line in rfile1.readlines():
	wfile.write(line)
for line in rfile2.readlines():
	wfile.write(line)

rfile1.close()
rfile2.close()
wfile.close()

rfile1 = open(dev_dir+"timit_dev_wav.scp","r")
rfile2 = open(dev_dir+"cu-chloe_dev_wav.scp","r")
wfile = open(dev_dir+"wav.scp","w")

for line in rfile1.readlines():
	wfile.write(line)
for line in rfile2.readlines():
	wfile.write(line)

rfile1.close()
rfile2.close()
wfile.close()

rfile1 = open(dev_dir+"timit_dev.text","r")
rfile2 = open(dev_dir+"cu-chloe_dev.ltext","r")
wfile = open(dev_dir+"text","w")

for line in rfile1.readlines():
	wfile.write(line)
for line in rfile2.readlines():
	wfile.write(line)

rfile1.close()
rfile2.close()
wfile.close()

rfile1 = open(dev_dir+"timit_dev.text","r")
rfile2 = open(dev_dir+"cu-chloe_dev.qtext","r")
wfile = open(dev_dir+"qdict","w")

for line in rfile1.readlines():
	wfile.write(line)
for line in rfile2.readlines():
	wfile.write(line)

rfile1.close()
rfile2.close()
wfile.close()
