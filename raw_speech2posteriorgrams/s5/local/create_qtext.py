#
# python local/create_qtext.py $all_qtext_dir $train_dir $test_dir $dev_dir
import sys

all_qtext_dir = sys.argv[1]+"/"
train_dir = sys.argv[2]+"/"
test_dir = sys.argv[3]+"/"
dev_dir = sys.argv[4]+"/"

rfile = open(all_qtext_dir+"all_cu-chloe.qtext","r")

lines = rfile.readlines()
rfile.close()

sentenceid_list = []

for line in lines:
	sentenceid_list.append(line.split(" ")[0])

train_wav_file = open(train_dir+"cu-chloe_train_wav.scp","r")
train_qtext_file = open(train_dir+"cu-chloe_train.qtext","w")

for line in train_wav_file.readlines():
	sentenceid = line.split(" ")[0]
	num = sentenceid_list.index(sentenceid)
	train_qtext_file.write(lines[num])

train_qtext_file.close()
train_wav_file.close()

test_wav_file = open(test_dir+"cu-chloe_test_wav.scp","r")
test_qtext_file = open(test_dir+"cu-chloe_test.qtext","w")

for line in test_wav_file.readlines():
	sentenceid = line.split(" ")[0]
	num = sentenceid_list.index(sentenceid)
	test_qtext_file.write(lines[num])

test_qtext_file.close()
test_wav_file.close()

dev_wav_file = open(dev_dir+"cu-chloe_dev_wav.scp","r")
dev_qtext_file = open(dev_dir+"cu-chloe_dev.qtext","w")

for line in dev_wav_file.readlines():
	sentenceid = line.split(" ")[0]
	num = sentenceid_list.index(sentenceid)
	dev_qtext_file.write(lines[num])

dev_qtext_file.close()
dev_wav_file.close()
