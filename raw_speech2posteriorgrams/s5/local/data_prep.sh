#!/bin/bash

TIMIT=$1
CU_CHLOE=$2

[ ! -d data ] && mkdir data
local=`pwd`/local
utils=`pwd`/utils
conf=`pwd`/conf

. ./path.sh # Needed for KALDI_ROOT
export PATH=$PATH:$KALDI_ROOT/tools/irstlm/bin
sph2pipe=$KALDI_ROOT/tools/sph2pipe_v2.5/sph2pipe
if [ ! -x $sph2pipe ]; then
	echo "Could not find (or execute) the sph2pipe program at $sph2pipe";
	exit 1;
fi

echo "Preparing TIMIT data!"
# First check if the train & test directories exist (these can either be upper-
# or lower-cased
if [ ! -d $TIMIT/TRAIN -o ! -d $TIMIT/TEST ] && [ ! -d $TIMIT/train -o ! -d $TIMIT/test ]; then
	echo "timit_data_prep.sh: Spot check of command line argument failed"
	echo "Command line argument must be absolute pathname to TIMIT directory"
	echo "with name like /export/corpora5/LDC/LDC93S1/timit/TIMIT"
	exit 1;
fi

# Now check what case the directory structure is
uppercased=false
train_dir=train
test_dir=test
if [ -d $TIMIT/TRAIN ]; then
	uppercased=true
	train_dir=TRAIN
	test_dir=TEST
fi

tmpdir=$(mktemp -d /tmp/kaldi.XXXX);
trap 'rm -rf "$tmpdir"' EXIT

# Get the list of speakers. The list of speakers in the 24-speaker core test 
# set and the 50-speaker development set must be supplied to the script. All
# speakers in the 'train' directory are used for training.
if $uppercased; then
	tr '[:lower:]' '[:upper:]' < $conf/dev_spk.list > $tmpdir/dev_spk
	tr '[:lower:]' '[:upper:]' < $conf/test_spk.list > $tmpdir/test_spk
	ls -d "$TIMIT"/TRAIN/DR*/* | sed -e "s:^.*/::" > $tmpdir/train_spk
else
	tr '[:upper:]' '[:lower:]' < $conf/dev_spk.list > $tmpdir/dev_spk
	tr '[:upper:]' '[:lower:]' < $conf/test_spk.list > $tmpdir/test_spk
	ls -d "$TIMIT"/train/dr*/* | sed -e "s:^.*/::" > $tmpdir/train_spk
fi

for x in train dev test; do
	[ ! -d data/${x} ] && mkdir data/${x}
	cd data/${x}
	# First, find the list of audio files (use only si & sx utterances).
	# Note: train & test sets are under different directories, but doing find on 
	# both and grepping for the speakers will work correctly.
	find $TIMIT/{$train_dir,$test_dir} -not \( -iname 'SA*' \) -iname '*.WAV' \
	| grep -f $tmpdir/${x}_spk > ${x}_sph.flist

	sed -e 's:.*/\(.*\)/\(.*\).WAV$:\1_\2:i' ${x}_sph.flist \
	> $tmpdir/${x}_sph.uttids
	paste $tmpdir/${x}_sph.uttids ${x}_sph.flist \
	| sort -k1,1 > ${x}_sph.scp

	cat ${x}_sph.scp | awk '{print $1}' > ${x}.uttids

	# Now, Convert the transcripts into our format (no normalization yet)
	# Get the transcripts: each line of the output contains an utterance 
	# ID followed by the transcript.
	find $TIMIT/{$train_dir,$test_dir} -not \( -iname 'SA*' \) -iname '*.PHN' \
	| grep -f $tmpdir/${x}_spk > $tmpdir/${x}_phn.flist
	sed -e 's:.*/\(.*\)/\(.*\).PHN$:\1_\2:i' $tmpdir/${x}_phn.flist \
	> $tmpdir/${x}_phn.uttids
	while read line; do
	[ -f $line ] || error_exit "Cannot find transcription file '$line'";
	cut -f3 -d' ' "$line" | tr '\n' ' ' | sed -e 's: *$:\n:'
	done < $tmpdir/${x}_phn.flist > $tmpdir/${x}_phn.trans
	paste $tmpdir/${x}_phn.uttids $tmpdir/${x}_phn.trans \
	| sort -k1,1 > ${x}.trans

	# Do normalization steps. 
	cat ${x}.trans | $local/timit_norm_trans.pl -i - -m $conf/phones.60-48-39.map -to 48 | sort > timit_${x}.text || exit 1;

	# Create wav.scp
	awk '{printf("%s '$sph2pipe' -f wav %s |\n", $1, $2);}' < ${x}_sph.scp > timit_${x}_wav.scp
	rm ${x}*
	cd ../..
done

echo "Preparing CU-CHLOE data!"
train_dir=data/train
test_dir=data/test
dev_dir=data/dev
all_qtext_dir=data

python local/create_wav_ltext.py $CU_CHLOE $train_dir $test_dir $dev_dir

python local/create_qtext.py $all_qtext_dir $train_dir $test_dir $dev_dir

echo "Combining TIMIT & CU-CHLOE data!"
python local/combine_timit_cu-chloe.py $train_dir $test_dir $dev_dir


echo "Creating utt2spk & spk2utt files! And then sort all files in each directory."
for x in train dev test; do
	python local/create_utt2spk.py data/$x
	utils/utt2spk_to_spk2utt.pl data/${x}/utt2spk > data/${x}/spk2utt
	local/fix_data_dir.sh data/${x}
	#utils/utt2spk_to_spk2utt.pl $data/utt2spk > $data/spk2utt
done
