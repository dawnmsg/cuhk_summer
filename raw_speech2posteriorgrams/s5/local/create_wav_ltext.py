#
# python local/create_wav_ltext.py $CU_CHLOE train_dir test_dir dev_dir
import sys
import os

cu_chloe = sys.argv[1]+"/"
Trans_dir = cu_chloe+"Transcription1/"
Recor_dir = cu_chloe+"Recordings/"
train_dir = sys.argv[2]+"/"
test_dir = sys.argv[3]+"/"
dev_dir = sys.argv[4]+"/"

train_wav_wfile = open(train_dir+"cu-chloe_train_wav.scp","w")
train_ltext_wfile = open(train_dir+"cu-chloe_train.ltext","w")
test_wav_wfile = open(test_dir+"cu-chloe_test_wav.scp","w")
dev_wav_wfile = open(dev_dir+"cu-chloe_dev_wav.scp","w")
dev_ltext_wfile = open(dev_dir+"cu-chloe_dev.ltext","w")
spk_file = open("conf/speaker_list_for_each_set.txt","r")

rfile = open("/home/leo/kaldi-master/egs/timit/s5/conf/phones.60-48-39.map","r")
phone_dict = {}
phn48_list = []
non_phn = []

for line in rfile.readlines():
	phn_list = line.split("	")
	phn_list[-1] = phn_list[-1][:-1]
	if len(phn_list) < 2:
		phn48_list.append(phn_list[0])
		phone_dict.update({phn_list[0]:"sil"})
	else:
		phn60, phn48 = line.split("	")[:2]
		phn48_list.append(phn48)
		phone_dict.update({phn60:phn48})

rfile.close()

# load speaker list for train and dev set
train_list = []
dev_list = []
for line in spk_file.readlines():
	if line.split(" ")[0][:-1] == "train_set":
		for spk in line.split(" ")[1:-1]:
			train_list.append(spk)
	if line.split(" ")[0][:-1] == "dev_set":
		for spk in line.split(" ")[1:-1]:
			dev_list.append(spk)

spk_file.close()

cate_list = os.listdir(Trans_dir)
cate_list.sort()

for cate in cate_list:
	people_list = os.listdir(Trans_dir+cate+"/")
	people_list.sort()
	for people in people_list:
		if people in train_list:
			sentence_list = os.listdir(Trans_dir+cate+"/"+people+"/")
			sentence_list.sort()
			for sentence in sentence_list:
				if sentence[-4:] == ".lab":
					rfile = open(Trans_dir+cate+"/"+people+"/"+sentence,"r")
					sentenceid = people+"_"+sentence[:-4]
					phn_list = []
					accept = True
					pre_phn = ""
					for line in rfile.readlines():
						phn = line.split(" ")[2]
						if phn == "sp":
							phn = "sil"
						if phn not in phn48_list:
							phn48 = phone_dict.get(phn)
							if phn48 == None:
								if phn not in non_phn:
									non_phn.append(phn)
								accept = False
								break
							else:
								phn = phn48
						if phn == "sil" and pre_phn == "sil":
							continue
						phn_list.append(phn)
						pre_phn = phn
					if accept == True:
						train_ltext_wfile.write(sentenceid+" ")
						for phn in phn_list:
							train_ltext_wfile.write(phn+" ")
						train_ltext_wfile.write("\n")
						train_wav_wfile.write(sentenceid+" "+cu_chloe+"Recordings/"+cate+"/"+people+"/"+sentence[:-3]+"wav"+" \n")
						rfile.close()
					else:
						test_wav_wfile.write(sentenceid+" "+cu_chloe+"Recordings/"+cate+"/"+people+"/"+sentence[:-3]+"wav"+" \n")
		elif people in dev_list:
			sentence_list = os.listdir(Trans_dir+cate+"/"+people+"/")
			sentence_list.sort()
			for sentence in sentence_list:
				if sentence[-4:] == ".lab":
					rfile = open(Trans_dir+cate+"/"+people+"/"+sentence,"r")
					sentenceid = people+"_"+sentence[:-4]
					phn_list = []
					accept = True
					pre_phn = ""
					for line in rfile.readlines():
						phn = line.split(" ")[2]
						if phn == "sp":
							phn = "sil"
						if phn not in phn48_list:
							phn48 = phone_dict.get(phn)
							if phn48 == None:
								if phn not in non_phn:
									non_phn.append(phn)
								accept = False
								break
							else:
								phn = phn48
						if phn == "sil" and pre_phn == "sil":
							continue
						phn_list.append(phn)
						pre_phn = phn
					if accept == True:
						dev_ltext_wfile.write(sentenceid+" ")
						for phn in phn_list:
							dev_ltext_wfile.write(phn+" ")
						dev_ltext_wfile.write("\n")
						dev_wav_wfile.write(sentenceid+" "+cu_chloe+"Recordings/"+cate+"/"+people+"/"+sentence[:-3]+"wav"+" \n")
						rfile.close()
					else:
						test_wav_wfile.write(sentenceid+" "+cu_chloe+"Recordings/"+cate+"/"+people+"/"+sentence[:-3]+"wav"+" \n")

train_wav_wfile.close()
train_ltext_wfile.close()
dev_wav_wfile.close()
dev_ltext_wfile.close()

non_phn_wfile = open("/home/leo/kaldi-master/egs/apm_train/s5/data/non_phn.txt","w")

for phn in non_phn:
	non_phn_wfile.write(phn+" \n")

non_phn_wfile.close()

cate_list = os.listdir(Recor_dir)
cate_list.sort()

for cate in cate_list:
	people_list = os.listdir(Recor_dir+cate+"/")
	people_list.sort()
	for people in people_list:
		if people in train_list or people in dev_list:
			sentence_list = os.listdir(Recor_dir+cate+"/"+people+"/")
			sentence_list.sort()
			for sentence in sentence_list:
				if sentence[-4:] == ".wav" and sentence[0] == "s":
					sentenceid = people+"_"+sentence[:-4]
					test_wav_wfile.write(sentenceid+" "+cu_chloe+"Recordings/"+cate+"/"+people+"/"+sentence+" \n")
		else:
			sentence_list = os.listdir(Recor_dir+cate+"/"+people+"/")
			sentence_list.sort()
			for sentence in sentence_list:
				if sentence[-4:] == ".wav":
					sentenceid = people+"_"+sentence[:-4]
					test_wav_wfile.write(sentenceid+" "+cu_chloe+"Recordings/"+cate+"/"+people+"/"+sentence+" \n")

test_wav_wfile.close()
