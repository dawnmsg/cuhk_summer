#this file is used to create utt2spk of CHLOE corpus 
import sys
fdir = sys.argv[1]+"/"
rfile = open(fdir+"wav.scp","r")
wfile = open(fdir+"utt2spk","w")

for line in rfile.readlines():
	utt = line.split(" ")[0]
	wfile.write(utt+" "+utt[:5]+"\n")

rfile.close()
wfile.close()