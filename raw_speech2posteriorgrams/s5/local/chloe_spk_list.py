#
# python local/chloe_spk_list.py conf
import sys

fdir = sys.argv[1]+"/"

wfile = open(fdir+"speaker_list_for_each_set.txt","w")

wfile.write("train_set: ")
for x in ["FM","MM","FC","MC"]:
	for i in range(3,38):
		if i < 10:
			wfile.write(x+"00"+str(i)+" ")
		else:
			wfile.write(x+"0"+str(i)+" ")

wfile.write("\n")

wfile.write("dev_set: ")
for x in ["FM","MM","FC","MC"]:
	for i in range(38,43):
		wfile.write(x+"0"+str(i)+" ")

wfile.write("\n")
wfile.close()
