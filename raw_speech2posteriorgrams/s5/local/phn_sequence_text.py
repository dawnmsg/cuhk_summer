#
import sys
dataset = sys.argv[1]+"/"
rfile = open("data/local/dict/lexicon.txt","r")
lex_dict = {}

for line in rfile.readlines():
	space = line.find(" ")
	key = line[:space]
	value = line[space+1:-1]
	lex_dict.update({key:value})

rfile.close()

rfile = open(dataset+"text","r")
wfile = open(dataset+"phn_text","w")

for line in rfile.readlines():
	txt = line.split(" ")[:-1]
	wfile.write(txt[0]+" ")
	for word in txt[1:]:
		phns = lex_dict.get(word)
		if phns == None:
			phns = "sil"
		wfile.write(phns+" ")
	wfile.write("\n")

rfile.close()
wfile.close()