#!/bin/bash

dir=$1

for file in `ls ./mfcc/raw*.ark`;do
  copy-feats ark:$file ark,t:./$dir/`echo $file|sed 's/\.\/mfcc\///g' | sed 's/ark/txt/g'`
done
