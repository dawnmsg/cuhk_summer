#!/bin/bash
# Copyright 2013-2015 Brno University of Technology (author: Karel Vesely)

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

# To be run from ../../
#
# Restricted Boltzman Machine (RBM) pre-training by Contrastive Divergence
# algorithm (CD-1). A stack of RBMs forms a Deep Belief Neetwork (DBN).
#
# This script by default pre-trains on plain features (ie. saved fMLLR features),
# building a 'feature_transformm' containing +/-5 frame splicem and global CMVN.
#
# There is also a support for adding speaker-based CMVN, deltas, i-vectors,
# or passing custom 'feature_transformm' or its prototype.
#

# Begin configuration.

# topology, initialization,
nn_depth=4             # number of hidden layers,
hid_dim=1024           # number of neurons per layer,
param_stddev_first=0.1 # init parameters in 1st RBM
param_stddev=0.1 # init parameters in other RBMs
input_vis_typem=gauss # type of visible nodes on DBN input
input_vis_typeq=bern

# number of iterations,
rbm_iter=1            # number of pre-training epochs (Gaussian-Bernoulli RBM has 2x more)

# pre-training opts,
rbm_lrate=0.4         # RBM learning rate
rbm_lrate_low=0.01    # lower RBM learning rate (for Gaussian units)
rbm_l2penalty=0.0002  # L2 penalty (increases RBM-mixing rate)
rbm_extra_opts=

# data processing,
copy_feats=true     # resave the features to tmpdirm,
copy_feats_tmproot=/tmp/kaldi.XXXX # sets tmproot for 'copy-feats',
copy_feats_compress=true # compress feats while resaving

# feature processing,
splicem=5            # (default) splicem features both-ways along time axis,
spliceq=0
cmvn_opts=          # (optional) adds 'apply-cmvn' to input feature pipeline, see opts,
delta_opts=         # (optional) adds 'add-deltas' to input feature pipeline, see opts,
ivector=            # (optional) adds 'append-vector-to-feats', the option is rx-filename for the 2nd stream,
ivector_append_tool=append-vector-to-feats # (optional) the tool for appending ivectors,

feature_transformm_proto= # (optional) use this prototype for 'feature_transformm',
feature_transformq_proto=
feature_transformm=  # (optional) directly use this 'feature_transformm',
feature_transformq=
feature_transform=

# misc.
verbose=1 # enable per-cache reports
skip_cuda_check=false

# End configuration.

echo "$0 $@"  # Print the command line for logging

[ -f path.sh ] && . ./path.sh;
. parse_options.sh || exit 1;

set -euo pipefail

if [ $# != 3 ]; then
   echo "Usage: $0 <datam> <exp-dir>"
   echo " e.g.: $0 datam/train exp/rbm_pretrain"
   echo "main options (for others, see top of script file)"
   echo "  --config <config-file>           # config containing options"
   echo ""
   echo "  --nn-depth <N>                   # number of RBM layers"
   echo "  --hid-dim <N>                    # number of hidden units per layer"
   echo "  --rbm-iter <N>                   # number of CD-1 iterations per layer"
   echo "                                   # can be used to subsample large datamsets"
   echo "  --rbm-lrate <float>              # learning-rate for Bernoulli-Bernoulli RBMs"
   echo "  --rbm-lrate-low <float>          # learning-rate for Gaussian-Bernoulli RBM"
   echo ""
   echo "  --cmvn-opts  <string>            # add 'apply-cmvn' to input feature pipeline"
   echo "  --delta-opts <string>            # add 'add-deltas' to input feature pipeline"
   echo "  --splicem <N>                     # splicem +/-N frames of input features"
   echo "  --copy-feats <bool>              # copy features to /tmp, lowers storage stress"
   echo ""
   echo "  --feature_transformm_proto <file> # use this prototype for 'feature_transformm'"
   echo "  --feature-transform <file>       # directly use this 'feature_transformm'"
   exit 1;
fi

datam=$1
dir=$2
dataq=$3

for f in $datam/feats.scp; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

#for f in $dataq/ali_train_binary.scp; do
#  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
#done

echo "# INFO"
echo "$0 : Pre-training Deep Belief Network as a stack of RBMs"
printf "\t dir       : $dir \n"
printf "\t Train-set : $datam '$(cat $datam/feats.scp | wc -l)'\n"
echo

[ -e $dir/${nn_depth}.dbn ] && echo "$0 Skipping, already have $dir/${nn_depth}.dbn" && exit 0

# check if CUDA compiled in and GPU is available,
if ! $skip_cuda_check; then cuda-gpu-available || exit 1; fi

mkdir -p $dir/log

###### PREPARE FEATURES ######
echo
echo "# PREPARING FEATURES"
if [ "$copy_feats" == "true" ]; then
  # re-save the features to local disk into /tmp/,
  tmpdirm=$(mktemp -d $copy_feats_tmproot)
  trap "echo \"# Removing features tmpdirm $tmpdirm @ $(hostname)\"; ls $tmpdirm; rm -r $tmpdirm" INT QUIT TERM EXIT
  copy-feats --compress=$copy_feats_compress scp:$datam/feats.scp ark,scp:$tmpdirm/train.ark,$dir/train_sortedm.scp || exit 1
  tmpdirq=$(mktemp -d $copy_feats_tmproot)
  #trap "echo \"# Removing features tmpdirq $tmpdirq @ $(hostname)\"; ls $tmpdirq; rm -r $tmpdirq" INT QUIT TERM EXIT
  #copy-feats --compress=$copy_feats_compress scp:$dataq/ali_train_binary.scp ark,scp:$tmpdirq/train.ark,$dir/train_sortedq.scp || exit 1
else
  # or copy the list,
  cp $datam/feats.scp $dir/train_sortedm.scp
  #cp $dataq/ali_train_binary.scp $dir/train_sortedq.scp
fi
# shuffle the list,
local/shuffle_list1.pl --srand 777 <$dir/train_sortedm.scp >$dir/trainm.scp

# create a 10k utt subset for global cmvn estimates,
head -n 10000 $dir/trainm.scp > $dir/trainm.scp.10k

head -n 10000 $dir/trainq.scp > $dir/trainq.scp.10k

head -n 10000 $dir/com_train.scp > $dir/com_train.scp.10k

# for debugging, add list with non-local features,
utils/shuffle_list.pl --srand 777 <$datam/feats.scp >$dir/trainm.scp_non_local


###### PREPARE FEATURE PIPELINE ######
# read the features
feats_trm="ark:copy-feats scp:$dir/trainm.scp ark:- |"

feats_trq="ark:copy-feats scp:$dir/trainq.scp ark:- |"

feats_tr="ark:copy-feats scp:$dir/com_train.scp ark:- |"

# get feature dim,
feat_dimm=$(feat-to-dim "$feats_trm" -)
feat_dimq=$(feat-to-dim "$feats_trq" -)
echo "# feature dim : $feat_dimm (input of 'feature_transformm')"
echo "# feature dim : $feat_dimq (input of 'feature_transformq')"

# Now we start building 'feature_transformm' which goes right in front of a NN.
# The forwarding is computed on a GPU before the frame shuffling is applied.
#
# Same GPU is used both for 'feature_transformm' and the NN training.
# So it has to be done by a single process (we are using exclusive mode).
# This also reduces the CPU-GPU uploads/downloads to minimum.
feature_transform=$dir/tr_combine_feature.nnet

if [ ! -z "$feature_transformm" ]; then
  echo "# importing 'feature_transformm' from '$feature_transformm'"
  tmpm=$dir/imported_$(basename $feature_transformm)
  cp $feature_transformm $tmpm; feature_transformm=$tmpm
else
  # Make default proto with splicem,
  if [ ! -z $feature_transformm_proto ]; then
    echo "# importing custom 'feature_transformm_proto' from : $feature_transformm_proto"
  else
    echo "+ default 'feature_transformm_proto' with splicem +/-$splicem frames"
    feature_transformm_proto=$dir/splicem${splicem}.proto
    echo "<Splice> <InputDim> $feat_dimm <OutputDim> $(((2*splicem+1)*feat_dimm)) <BuildVector> -$splicem:$splicem </BuildVector>" >$feature_transformm_proto
  fi

  # Initialize 'feature-transform' from a prototype,
  feature_transformm=$dir/tr_$(basename $feature_transformm_proto .proto).nnet
  nnet-initialize --binary=false $feature_transformm_proto $feature_transformm

  # Renormalize the MLP input to zero mean and unit variance,
  feature_transformm_old=$feature_transformm
  feature_transformm=${feature_transformm%.nnet}_cmvn-g.nnet
  echo "# compute normalization stats from 10k sentences"
  nnet-forward --print-args=true --use-gpu=yes $feature_transformm_old \
    "$(echo $feats_trm | sed 's|trainm.scp|trainm.scp.10k|')" ark:- |\
    compute-cmvn-stats ark:- $dir/cmvn-gm.stats
  echo "# + normalization of NN-input at '$feature_transformm'"
  nnet-concat --print-args=false --binary=false $feature_transformm_old \
    "cmvn-to-nnet $dir/cmvn-gm.stats -|" $feature_transformm
fi


if [ ! -z "$feature_transformq" ]; then
  echo "# importing 'feature_transformq' from '$feature_transformq'"
  tmpq=$dir/imported_$(basename $feature_transformq)
  cp $feature_transformq $tmpq; feature_transformq=$tmpq
else
  # Make default proto with spliceq,
  if [ ! -z $feature_transformq_proto ]; then
    echo "# importing custom 'feature_transformq_proto' from : $feature_transformq_proto"
  else
    echo "+ default 'feature_transformq_proto' with spliceq +/-$spliceq frames"
    feature_transformq_proto=$dir/spliceq${spliceq}.proto
    echo "<Splice> <InputDim> $feat_dimq <OutputDim> $(((2*spliceq+1)*feat_dimq)) <BuildVector> -$spliceq:$spliceq </BuildVector>" >$feature_transformq_proto
  fi

  # Initialize 'feature-transform' from a prototype,
  feature_transformq=$dir/tr_$(basename $feature_transformq_proto .proto).nnet
  nnet-initialize --binary=false $feature_transformq_proto $feature_transformq

fi

###### Show the final 'feature_transformm' in the log,
echo
echo "### Showing the final 'feature_transformm':"
nnet-info $feature_transformm
echo "###"

echo
echo "### Showing the final 'feature_transformq':"
nnet-info $feature_transformq
echo "###"

echo
echo "### Showing the final 'feature_transform':"
nnet-initialize --print-args=false <(echo "<ParallelComponent> <InputDim> $(($feat_dimm+$feat_dimq)) <OutputDim> $(((2*spliceq+1)*feat_dimq+(2*splicem+1)*feat_dimm)) <NestedNnetFilename> $feature_transformm $feature_transformq </NestedNnetFilename>") $feature_transform
nnet-info $feature_transform
echo "###"

###### MAKE LINK TO THE FINAL feature_transformm, so the other scripts will find it ######
[ -f $dir/final.feature_transformm ] && unlink $dir/final.feature_transformm
(cd $dir; ln -s $(basename $feature_transformm) final.feature_transformm )
feature_transformm=$dir/final.feature_transformm

[ -f $dir/final.feature_transformq ] && unlink $dir/final.feature_transformq
(cd $dir; ln -s $(basename $feature_transformq) final.feature_transformq )
feature_transformq=$dir/final.feature_transformq

[ -f $dir/final.feature_transform ] && unlink $dir/final.feature_transform
(cd $dir; ln -s $(basename $feature_transform) final.feature_transform )
feature_transform=$dir/final.feature_transform


###### GET THE DIMENSIONS ######
num_feam=$(feat-to-dim --print-args=false "$feats_trm nnet-forward --use-gpu=no $feature_transformm ark:- ark:- |" - 2>/dev/null)
num_hid=$hid_dim

num_feaq=$(feat-to-dim --print-args=false "$feats_trq nnet-forward --use-gpu=no $feature_transformq ark:- ark:- |" - 2>/dev/null)

###### PERFORM THE PRE-TRAINING ######
for depth in $(seq 1 $nn_depth); do
  echo
  echo "# PRE-TRAINING RBM LAYER $depth"
  RBM=$dir/$depth.rbm
  [ -f $RBM ] && echo "RBM '$RBM' already trained, skipping." && continue
  # The first RBM needs special treatment, because of Gussian input nodes,
  if [ "$depth" == "1" ]; then
    # This is usually Gaussian-Bernoulli RBM (not if CNN layers are part of input transform)
    # initialize,
    echo "# initializing '$RBM.init'"
    RBMm=$dir/m$depth.rbm
    RBMq=$dir/q$depth.rbm
    [ -f $RBMm ] && [ -f $RBMq ] && echo "RBM '$RBMm' '$RBMq' already trained, skipping." && continue
    echo "<Rbm> <InputDim> $num_feam <OutputDim> $num_hid <VisibleType> $input_vis_typem <HiddenType> bern <ParamStddev> $param_stddev_first" > $RBMm.proto
    nnet-initialize $RBMm.proto $RBMm.init 2>$dir/log/nnet-initialize.m$depth.log || exit 1
    # pre-train,
    num_iterm=$rbm_iter; [ $input_vis_typem == "gauss" ] && num_iterm=$((2*rbm_iter)) # 2x more epochs for Gaussian input
    [ $input_vis_typem == "bern" ] && rbm_lrate_low=$rbm_lrate # original lrate for Bernoulli input
    echo "# pretraining '$RBMm' (input $input_vis_typem, lrate $rbm_lrate_low, iters $num_iterm)"
    rbm-train-cd1-frmshuff --learn-rate=$rbm_lrate_low --l2-penalty=$rbm_l2penalty \
      --num-iters=$num_iterm --verbose=$verbose \
      --feature-transform=$feature_transformm \
      $rbm_extra_opts \
      $RBMm.init "$feats_trm" $RBMm 2>$dir/log/rbm.m$depth.log || exit 1

    echo "<Rbm> <InputDim> $num_feaq <OutputDim> $num_hid <VisibleType> $input_vis_typeq <HiddenType> bern <ParamStddev> $param_stddev_first" > $RBMq.proto
    nnet-initialize $RBMq.proto $RBMq.init 2>$dir/log/nnet-initialize.q$depth.log || exit 1
    # pre-train,
    num_iter=$rbm_iter; [ $input_vis_typem == "gauss" ] && num_iter=$((2*rbm_iter)) # 2x more epochs for Gaussian input
    [ $input_vis_typem == "bern" ] && rbm_lrate_low=$rbm_lrate # original lrate for Bernoulli input
    echo "# pretraining '$RBMq' (input $input_vis_typeq, lrate $rbm_lrate_low, iters $num_iter)"
    rbm-train-cd1-frmshuff --learn-rate=$rbm_lrate_low --l2-penalty=$rbm_l2penalty \
      --num-iters=$num_iter --verbose=$verbose \
      --feature-transform=$feature_transformq \
      $rbm_extra_opts \
      $RBMq.init "$feats_trq" $RBMq 2>$dir/log/rbm.q$depth.log || exit 1
  elif [[ "$depth" == "2" ]]; then
    echo "# computing cmvn stats '$dir/$depth.cmvn' for RBM initialization"
    if [ ! -f $dir/$depth.cmvn ]; then
      nnet-forward --print-args=false --use-gpu=yes \
        "nnet-concat $feature_transform $dir/$((depth-1)).dbn - |" \
        "$(echo $feats_tr | sed 's|com_train.scp|com_train.scp.10k|')" ark:- | \
      compute-cmvn-stats --print-args=false ark:- - | \
      cmvn-to-nnet --print-args=false - $dir/$depth.cmvn || exit 1
    else
      echo "# compute-cmvn-stats already done, skipping."
    fi
    # initialize,
    echo "initializing '$RBM.init'"
    echo "<Rbm> <InputDim> $(($num_hid+$num_hid)) <OutputDim> $num_hid <VisibleType> bern <HiddenType> bern <ParamStddev> $param_stddev <VisibleBiasCmvnFilename> $dir/$depth.cmvn" > $RBM.proto
    nnet-initialize $RBM.proto $RBM.init 2>$dir/log/nnet-initialize.$depth.log || exit 1
    # pre-train,
    echo "pretraining '$RBM' (lrate $rbm_lrate, iters $rbm_iter)"
    rbm-train-cd1-frmshuff --learn-rate=$rbm_lrate --l2-penalty=$rbm_l2penalty \
      --num-iters=$rbm_iter --verbose=$verbose \
      --feature-transform="nnet-concat $feature_transform $dir/$((depth-1)).dbn - |" \
      $rbm_extra_opts \
      $RBM.init "$feats_tr" $RBM 2>$dir/log/rbm.$depth.log || exit 1
  else
    # This is Bernoulli-Bernoulli RBM,
    # cmvn stats for init,
    echo "# computing cmvn stats '$dir/$depth.cmvn' for RBM initialization"
    if [ ! -f $dir/$depth.cmvn ]; then
      nnet-forward --print-args=false --use-gpu=yes \
        "nnet-concat $feature_transform $dir/$((depth-1)).dbn - |" \
        "$(echo $feats_tr | sed 's|com_train.scp|com_train.scp.10k|')" ark:- | \
      compute-cmvn-stats --print-args=false ark:- - | \
      cmvn-to-nnet --print-args=false - $dir/$depth.cmvn || exit 1
    else
      echo "# compute-cmvn-stats already done, skipping."
    fi
    # initialize,
    echo "initializing '$RBM.init'"
    echo "<Rbm> <InputDim> $num_hid <OutputDim> $num_hid <VisibleType> bern <HiddenType> bern <ParamStddev> $param_stddev <VisibleBiasCmvnFilename> $dir/$depth.cmvn" > $RBM.proto
    nnet-initialize $RBM.proto $RBM.init 2>$dir/log/nnet-initialize.$depth.log || exit 1
    # pre-train,
    echo "pretraining '$RBM' (lrate $rbm_lrate, iters $rbm_iter)"
    rbm-train-cd1-frmshuff --learn-rate=$rbm_lrate --l2-penalty=$rbm_l2penalty \
      --num-iters=$rbm_iter --verbose=$verbose \
      --feature-transform="nnet-concat $feature_transform $dir/$((depth-1)).dbn - |" \
      $rbm_extra_opts \
      $RBM.init "$feats_tr" $RBM 2>$dir/log/rbm.$depth.log || exit 1
  fi

  # Create DBN stack,
  if [ "$depth" == "1" ]; then
    echo "# converting RBMm, RBMq to $dir/m$depth.dbn, $dir/q$depth.dbn"
    rbm-convert-to-nnet $RBMm $dir/m$depth.dbn
    rbm-convert-to-nnet $RBMq $dir/q$depth.dbn
    nnet-initialize --print-args=false <(echo "<ParallelComponent> <InputDim> $(($num_feam+$num_feaq)) <OutputDim> $(($num_hid+$num_hid)) <NestedNnetFilename> $dir/m$depth.dbn $dir/q$depth.dbn </NestedNnetFilename>") $dir/$depth.dbn
  else
    echo "# appending RBM to $dir/$depth.dbn"
    nnet-concat $dir/$((depth-1)).dbn "rbm-convert-to-nnet $RBM - |"  $dir/$depth.dbn
  fi

done

echo
echo "# REPORT"
echo "# RBM pre-training progress (line per-layer)"
grep progress $dir/log/rbm.*.log
echo

echo "Pre-training finished."

sleep 3
exit 0
